// console.log("Hello");

//Function able to receive data without the use of global variables or prompt()

// "name" is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked

function printName(name){
	console.log("My name is " + name);
};

//Data passed into a funtion invocation can be received by the function
// This is what we call an argument

printName("Mae");
printName("Mojo");

function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

//check divisibility reusable using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);


function myFavoriteSuperhero(hero){
	console.log("My favorite Superhero is: " + hero);
};

myFavoriteSuperhero("Captain America");

function oddAndEvenNumbers(num){
	let even = 28;
	console.log("The number " + num + " is an even number");

	let evenNumber = even === 28;
	console.log("Is " + num + " an even number?")
	console.log(evenNumber);
};

oddAndEvenNumbers(28);

// Multiple Arguments can also be passed into a function; nulitple parameters can contain our arguments

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial + ' ' + lastName);
};

printFullName('Juan', 'Crisostomo', 'Ibarra');
printFullName('Ibarra', 'Juan', 'Crisostomo');

/*

	Parameters will contain the argument according to the order it was passed.

	In other language, providing more/less argumentss that the expected paramters sometimes causes an error or changes the behavior of the function

*/

printFullName('Stephen', 'Wardell');
printFullName('Stephen', 'Wardell', 'Curry', 'Thomas');
printFullName('Stephen', 'Wardell', 'Curry');

// Use variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);

function myFavoriteSongs(song1, song2, song3, song4, song5){
	console.log("My favorite songs are: "); 

	console.log("1. " + song1);
	console.log("2. " + song2);
	console.log("3. " + song3);
	console.log("4. " + song4);
	console.log("5. " + song5);


	// return song1 + song2 + song3 + song4 + song5;
	// return "Hello, sir Rupert";
};

myFavoriteSongs('Stan', 'Till I Collapse', 'Venom', 'Without Me', 'Lose Yourself');

// Return Statement
// Currently, or so far, our function are able to display data in our console
// However, our function cannot yet return values. Function are able to return values which can be saved into a variable using the return statement/keyword.

let fullName = printFullName('Gabriel', 'Vernadette', 'Fernandez');
console.log(fullName);//undefined

function returnFullName(firstName, MiddleName, lastName){
	return firstName + ' ' + MiddleName + ' ' + lastName;
}

fullName = returnFullName("Juan", "Ponce", "Enrile");
// printFullName function(); return undefined becasue the function does not have return statement
//returnFullName(); return a string as a value because it has a return statement
console.log(fullName);

//funtcions which have a return statement are able to return value and it can be saved in a variable
console.log(fullName + ' is my grandpa');

function returnPhilippinesAddress(city){
	return  city + ", Philippines.";
};

//return - returned a value from a function which we can save in a variable
let myFullAddress = returnPhilippinesAddress("San Perdo");
console.log(myFullAddress);

// return true if number is divisible by 4, else returns false
function checkDivisibilityBy4(number){
	let remainder = number % 4;
	let isDivisibleBy4 = remainder === 0;

	//returned either true of false
	//Not only can you return raw values/data, we can also directly return a value
	return isDivisibleBy4;
	// return keyword not only allows us to return value but also ends the process of the function
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
// let num14isDivisibleBy4 = checkDivisibilityBy4(14);
// num4isDivisibleBy4();
// checkDivisibilityBy4(4);
// console.log(num4isDivisibleBy4);
// console.log(num14isDivisibleBy4);

function createPlayreInfo(username, level, job){
	return('username: ' + username + ' level: ' + level + ' Job: ' + job);
};

let user1 = createPlayreInfo('white_night', 95, 'Paladin');
console.log(user1);



function additionActivity(number1, number2){
	let result = number1 + number2;

	return result;
};

let sum = additionActivity(20, 4);
console.log(sum);


function productMiniActivity(number3, number4){
	return (number3 * number4);

};

let product = productMiniActivity(45, 90);
console.log(product);

function areaOfaCircle(radius, pi){
	return area = pi * (radius**2);
};

let radiusOfTheCircle = areaOfaCircle(6, 3.1416);
console.log(radiusOfTheCircle);


/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

console.log("Assignment:");

function passingPercentage(x, y){
	let passingGrade = (x / y) * 100;
	let isPassed = passingGrade >= 75;

	console.log("My Score is: " + x);
	console.log("The Total Score is: " + y)
	console.log("Percentage is: " + passingGrade + "%");

	return isPassed;

};


let isPassingScore = passingPercentage(123, 300);
console.log("Did my score passed? ")
console.log(isPassingScore);
